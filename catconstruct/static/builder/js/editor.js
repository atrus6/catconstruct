/*global THREE*/
/*global WEBGL*/
/*global Cookies*/

if ( WEBGL.isWebGLAvailable() === false ) {

	document.body.appendChild( WEBGL.getWebGLErrorMessage() );

}

var camera, scene, renderer, controls;
var plane, cube;
var mouse, raycaster, isShiftDown = false;

var rollOverMesh, rollOverMaterial;
var cubeGeo;
var cubeMaterial, bedMaterial, hutMaterial;
var current_type = 'cube';

var pane = document.getElementById('3pane');

var objects = [];

init();
animate();

function init() {
	camera = new THREE.PerspectiveCamera( 45, pane.offsetWidth / pane.offsetHeight, 1, 10000 );
	camera.position.set( 500, 800, 1300 );
	camera.lookAt( 0, 0, 0 );

	controls = new THREE.OrbitControls(camera, pane);
	// controls.update()
	// controls.rotateSpeed = 1.0;
	// controls.zoomSpeed = 1.2;
	// controls.panSpeed = 0.8;
	// controls.noZoom = false;
	// controls.noPan = false;
	// //controls.staticMoving = true;
	// controls.dynamicDampingFactor = 0.3;
	// controls.keys = [ 65, 83, 68 ];
	// controls.addEventListener( 'change', render );

	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xf0f0f0 );

	// roll-over helpers

	var rollOverGeo = new THREE.BoxBufferGeometry( 50, 50, 50 );
	rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );
	rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );
	scene.add( rollOverMesh );

	// cubes

	cubeGeo = new THREE.BoxBufferGeometry( 50, 50, 50 );
	cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c, map: new THREE.TextureLoader().load( '/static/builder/img/cube.png' ) } );
	bedMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c, map: new THREE.TextureLoader().load( '/static/builder/img/bed.png' ) } );
	hutMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c, map: new THREE.TextureLoader().load( '/static/builder/img/hut.png' ) } );

	// grid

	var gridHelper = new THREE.GridHelper( 1000, 20 );
	scene.add( gridHelper );

	raycaster = new THREE.Raycaster();
	mouse = new THREE.Vector2();

	var geometry = new THREE.PlaneBufferGeometry( 1000, 1000 );
	geometry.rotateX( - Math.PI / 2 );

	plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
	scene.add( plane );

	objects.push( plane );

	// lights
	var ambientLight = new THREE.AmbientLight( 0x606060 );
	scene.add( ambientLight );

	var directionalLight = new THREE.DirectionalLight( 0xffffff );
	directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
	scene.add( directionalLight );

	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( pane.offsetWidth, window.innerHeight );
	renderer.setAnimationLoop(animate);
	document.getElementById('3pane').appendChild(renderer.domElement);

	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	document.addEventListener('touchend', onDocumentTouchUp, false);
	document.addEventListener( 'mousedown', onDocumentMouseDown, false );
	document.addEventListener( 'keydown', onDocumentKeyDown, false );
	document.addEventListener( 'keyup', onDocumentKeyUp, false );
	
	document.getElementById('add_to_cart').addEventListener('click', add_to_cart);
	document.getElementById('ctrlBtn').addEventListener('click', toggleControls);
	document.getElementById('add_to_cart').addEventListener('touchend', add_to_cart, true);
	document.getElementById('ctrlBtn').addEventListener('touchend', toggleControls, true);

	window.addEventListener( 'resize', onWindowResize, false );
	loadTree();
	onWindowResize();
}

function loadTree() {
	var csrftoken = Cookies.get('csrftoken');
	var xmlHttp = new XMLHttpRequest();
	var data = new FormData();
	
	xmlHttp.onreadystatechange = function()  {
		if(this.readyState == 4 && this.status == 200) {
			var obj = JSON.parse(this.responseText);
			obj = JSON.parse(obj['items']);
		
		    for(var i = 0; i < obj.length; i++) {
		    	var current_type = obj[i][0];
		    	var voxel;
		    	
		    	if(current_type == 'cube') {
					voxel = new THREE.Mesh(cubeGeo, cubeMaterial);
				} else if (current_type == 'bed') {
					voxel = new THREE.Mesh(cubeGeo, bedMaterial);
				} else if(current_type == 'hut') {
					voxel = new THREE.Mesh(cubeGeo, hutMaterial);
				}
				console.log('obj[1]', obj[i][1])
				
				voxel.position.x = obj[i][1] * 50 + 25;
				voxel.position.y = obj[i][2] * 50 + 25;
				voxel.position.z = obj[i][3] * 50 + 25;
				
				scene.add(voxel);
		    }
		}
	}
	data.append('csrfmiddlewaretoken', csrftoken);
	xmlHttp.open("post", "/json/cube_list"); 
	xmlHttp.send(data);
	
	// if(current_type == 'cube') {
	// 	voxel = new THREE.Mesh(cubeGeo, cubeMaterial);
	// } else if (current_type == 'bed') {
	// 	voxel = new THREE.Mesh(cubeGeo, bedMaterial);
	// } else if(current_type == 'hut') {
	// 	voxel = new THREE.Mesh(cubeGeo, hutMaterial);
	// }
			
	// voxel.position.copy( intersect.point ).add( intersect.face.normal );
	// voxel.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
	// scene.add(voxel);

	// objects.push( voxel );
}

function onWindowResize() {

	camera.aspect = pane.offsetWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( pane.offsetWidth, window.innerHeight );

}

function getX(e) {
	var rect = pane.getBoundingClientRect();
	return ((e.clientX-rect.left)/pane.childNodes[0].offsetWidth) * 2 - 1;
}

function getY(e) {
	var rect = pane.getBoundingClientRect();
	return (- ((e.clientY-rect.top) / pane.childNodes[0].offsetHeight ) * 2 + 1) ;
}

function onDocumentMouseMove( event ) {
	mouse.set(getX(event), getY(event));

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( objects );

	if ( intersects.length > 0 ) {
		var intersect = intersects[intersects.length-1];

		rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
		rollOverMesh.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
	}

	render();

}

function onDocumentTouchMove(event) {
	var rect = pane.getBoundingClientRect();

	mouse.set( ( (event.clientX-rect.left) / (pane.offsetWidth) ) * 2 - 1, - ( (event.clientY-rect.top) / pane.offsetHeight ) * 2 + 1 );

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( objects );

	if ( intersects.length > 0 ) {
		var intersect = intersects[0];
		

		rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
		rollOverMesh.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
	}

	render();
}

var rsc = function()  {
	if(this.readyState == 4 && this.status == 200) {
		var obj = JSON.parse(this.responseText);
	
	    document.getElementById('pipe-li').textContent = "Pipes: " + obj['pipe'];
	    document.getElementById('corner-li').textContent = "Corners: " + obj['corner'];
	    document.getElementById('4way-li').textContent = "4-Way: " + obj['bottom_4way'];
	    document.getElementById('tway-li').textContent = "T-Way: " + obj['bottom_tway'];
	    document.getElementById('6way-li').textContent = "6-Way: " + obj['6way'];
	    document.getElementById('topper-li').textContent = "Toppers: " + obj['topper'];
	    document.getElementById('bed-li').textContent = "Bed: " + obj['bed'];
	    document.getElementById('hut-li').textContent = "Hut: " + obj['hut'];
	    document.getElementById('scratcher-li').textContent = "Scratchers: " + obj['scratcher'];
	}
}

function userPress(mouse) {
	var voxel;
	var csrftoken = Cookies.get('csrftoken');
	var xmlHttp = new XMLHttpRequest();
	var data = new FormData();
	
	raycaster.setFromCamera( mouse, camera );
	var intersects = raycaster.intersectObjects( objects );

	if (intersects.length > 0) {
		var intersect = intersects[0];

		// delete cube
		if ( isShiftDown ) {
			if ( intersect.object !== plane ) {
				scene.remove( intersect.object );
				objects.splice( objects.indexOf( intersect.object ), 1 );
				
				voxel = new THREE.Mesh(cubeGeo, cubeMaterial);
				voxel.position.copy( intersect.point ).add( intersect.face.normal );
				voxel.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
            
	            data.append('x', Math.floor(voxel.position.x/50));
	            data.append('y', Math.floor(voxel.position.y/50));
	            data.append('z', Math.floor(voxel.position.z/50)-1);
	            data.append('csrfmiddlewaretoken', csrftoken);

	        	xmlHttp.onreadystatechange = rsc;
	        	xmlHttp.open("post", "/json/remove"); 
	        	xmlHttp.send(data); 
			}
		// create cube
		} else {
			if(intersect.object !== plane) {
				console.log('Clicking Cube.');
				
				return;
			}
			if(current_type == 'cube') {
				voxel = new THREE.Mesh(cubeGeo, cubeMaterial);
			} else if (current_type == 'bed') {
				voxel = new THREE.Mesh(cubeGeo, bedMaterial);
			} else if(current_type == 'hut') {
				voxel = new THREE.Mesh(cubeGeo, hutMaterial);
			}
			
			voxel.position.copy( intersect.point ).add( intersect.face.normal );
			voxel.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
			console.log('vox pos', voxel.position);
			scene.add(voxel);

			objects.push( voxel );
			
            data.append('type', current_type);
            data.append('x', Math.floor(voxel.position.x/50));
            data.append('y', Math.floor(voxel.position.y/50));
            data.append('z', Math.floor(voxel.position.z/50));
            data.append('csrfmiddlewaretoken', csrftoken);
            
        	xmlHttp.onreadystatechange = rsc;
        	xmlHttp.open("post", "/json/add"); 
        	xmlHttp.send(data); 
		}
		render();
	}
}

function onDocumentTouchUp(event) {
	mouse.set(getX(event.changedTouches[0]), getY(event.changedTouches[0]));
	
	userPress(mouse);
}

function onDocumentMouseDown( event ) {
	var rect = pane.getBoundingClientRect();

	controls.enabled = false;

	if(event.button != 0) {
		return;
	}

	if(event.ctrlKey) {
		controls.enabled = true;
		return;
	}

	mouse.set(getX(event), getY(event));
	userPress(mouse);
	
}

function onDocumentKeyDown( event ) {

	switch ( event.keyCode ) {

		case 16: isShiftDown = true; break;

	}

}

function onDocumentKeyUp( event ) {

	switch ( event.keyCode ) {

		case 16: isShiftDown = false; break;

	}

}

function animate() {
	render();
	controls.update();
}

function render() {
	renderer.render( scene, camera );
}

function change_type(which) {
	current_type = which;
}

function reset_tree() {
	
}

function add_to_cart() {
	var csrftoken = Cookies.get('csrftoken');
	var data = new FormData();
	data.append('csrfmiddlewaretoken', csrftoken);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function()
	{
    	if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    	{
        	console.log('Added.');
    	}
	}
	xmlHttp.open("post", "/json/add_tree_to_cart"); 
	xmlHttp.send(data); 
}

function toggleControls() {
	document.getElementById("ui").classList.toggle("collapsed");

	var b = document.getElementById('ctrlBtn');
			
	if(b.textContent == 'Hide Controls') {
		b.textContent = 'Show Controls';
	} else {
		b.textContent = 'Hide Controls'
	}
}
