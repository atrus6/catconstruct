from django.urls import path
from django.views.generic import TemplateView

from .views import AccessoriesPage, CartPage, ShippingPage, CheckoutPage
from .views import add_cube, add_to_cart, reset_tree, signup, add_tree_to_cart
from .views import get_cube_list
from .views import change_cart_quantity, remove_cart_item
from .views import remove_cube

urlpatterns = [
        path('', TemplateView.as_view(template_name='builder/index.html'), name='index'),
        path('builder/', TemplateView.as_view(template_name='builder/builder.html'), name='builder'),
        path('features/', TemplateView.as_view(template_name='builder/features.html'), name='features'),
        path('accessories/', AccessoriesPage.as_view(), name='accessories'),
        path('shipping/', ShippingPage.as_view(), name='shipping'),
        path('checkout/', CheckoutPage.as_view(), name='checkout'),
        path('cart/', CartPage.as_view(), name='cart'),
        path('json/add', add_cube),
        path('json/add_to_cart', add_to_cart),
        path('json/add_tree_to_cart', add_tree_to_cart),
        path('json/cube_list', get_cube_list),
        path('json/change_cart_quantity/<int:pk>/<int:quantity>', change_cart_quantity, name='change_quantity'),
        path('json/remove_cart_item/<int:pk>', remove_cart_item, name='remove_item'),
        path('json/remove', remove_cube),
        path('json/reset', reset_tree, name='reset'),
        path('accounts/register/', signup, name='register'),
        path('accounts/profile/', TemplateView.as_view(template_name='registration/profile.html'), name='profile'),
        path('q/', TemplateView.as_view(template_name='builder/svg_test.html')),
        ]
