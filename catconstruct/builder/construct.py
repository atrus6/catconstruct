import json

import networkx as nx


class Tree:
    cubes = nx.Graph()
    accessories = []
    cube_list = []
    
    def create_node_name(self, x, y, z):
        return 'x:' + str(x) + ' y:' + str(y) + ' z:' + str(z)
    
    def clear(self):
        self.cubes = nx.Graph()
        self.accessories = []
        self.cube_list = []

    def get_node_by_coord(self, x, y, z):
        for nid, attrs in self.cubes.node.data():
            if attrs.get('x') == x and attrs.get('y') == y and attrs.get('z') == z:
                return nid
                
        return False
    
    def add_node(self, x, y, z):
        if not self.get_node_by_coord(x, y, z):
            self.cubes.add_node(self.create_node_name(x,y,z), x=x, y=y, z=z)
            return self.create_node_name(x,y,z)
        else:
            return self.get_node_by_coord(x, y, z)
    
    def add_cube(self, x, y, z):
        a = self.add_node(x, y, z)
        b = self.add_node(x+1, y, z)
        e = self.add_node(x, y, z+1)
        f = self.add_node(x+1, y, z+1)
        c = self.add_node(x, y+1, z)
        d = self.add_node(x+1, y+1, z)
        g = self.add_node(x, y+1, z+1)
        h = self.add_node(x+1, y+1, z+1)

        self.cubes.add_edge(a, e)
        self.cubes.add_edge(a, c)
        self.cubes.add_edge(b, d)
        self.cubes.add_edge(a, b)
        self.cubes.add_edge(b, f)
        self.cubes.add_edge(c, d)
        self.cubes.add_edge(c, g)
        self.cubes.add_edge(d, h)
        self.cubes.add_edge(e, f)
        self.cubes.add_edge(e, g)
        self.cubes.add_edge(f, h)
        self.cubes.add_edge(g, h)

    def corner_type(self, node):
        if len(self.cubes.edges(node)) == 6:
            return '6way'
        elif len(self.cubes.edges(node)) == 3:
            return 'corner'
        elif len(self.cubes.edges(node)) == 4:
            return 'bottom_tway'
        elif len(self.cubes.edges(node)) == 5:
            return 'bottom_4way'

    def calculate_corner_types(self):
        for x in self.cubes.nodes:
            self.cubes.nodes[x]['type'] = self.corner_type(x)

    def get_corner_type_counts(self, t):
        rv = 0

        for x in self.cubes.nodes:
            if self.cubes.nodes[x]['type'] == t:
                rv = rv + 1

        return rv

    def get_accessory_counts(self, acc):
        rv = 0

        for a in self.accessories:
            if a['type'] == acc:
                rv = rv + 1

        return rv
    
    def add(self, kind, x, y, z):
        if None in [kind, x, y, z]:
            raise Exception('None given in params.')
        x = int(x)
        y = int(y)
        z = int(z)
        
        self.cube_list.append((kind, x, y, z))
        
        if kind == 'bed':
            self.add_cube(x, y, z)
            self.accessories.append({'type':'bed', 'x':x, 'y':y, 'z':z})
        elif kind == 'cube':
            self.add_cube(x, y, z)
            self.accessories.append({'type':'topper', 'x':x, 'y':y, 'z':z})
        elif kind == 'hut':
            self.add_cube(x, y, z)
            self.accessories.append({'type': 'hut', 'x':x, 'y':y, 'z':z})
        elif kind == 'ramp':
            a = self.add_node(x,y,z)
            b = self.add_node(x+1, y, z)
            c = self.add_node(x, y, z+1)
            d = self.add_node(x+1, y, z+1)
            e = self.add_node(x, y+1, z+1)
            f = self.add_node(x+1, y+1, z+1)

            self.cubes.add_edge(a, b)
            self.cubes.add_edge(a, c)
            self.cubes.add_edge(b, d)
            self.cubes.add_edge(c, d)
            self.cubes.add_edge(c, e)
            self.cubes.add_edge(d, f)
            self.cubes.add_edge(e, f)

            self.accessories.append({'type':'ramp_pipe', 'x':x, 'y':y, 'z':z})
            self.accessories.append({'type':'ramp_pipe','x':x, 'y':y, 'z':z})
            self.accessories.append({'type':'ramp_fabric','x':x, 'y':y, 'z':z})
        else:
            raise Exception('Invalid Cube Type')
            
        self.calculate_corner_types()
        
    def determine(self, cube, x, y, z):
        return cube[1] == x and cube[2] == y and cube[3] == z
        
    def remove(self, x, y, z):
        self.cube_list[:] = [cube for cube in self.cube_list if self.determine(cube, x, y, z)]
        
        x = int(x)
        y = int(y)
        z = int(z)
        
        l = [self.get_node_by_coord(x, y, z)]
        l.append(self.get_node_by_coord(x+1, y, z))
        l.append(self.get_node_by_coord(x, y, z+1))
        l.append(self.get_node_by_coord(x+1, y, z+1))
        l.append(self.get_node_by_coord(x, y+1, z))
        l.append(self.get_node_by_coord(x+1, y+1, z))
        l.append(self.get_node_by_coord(x, y+1, z+1))
        l.append(self.get_node_by_coord(x+1, y+1, z+1))
        
        for i in l:
            try:
                if len(list(self.cubes.adj[i])) <= 3:
                    self.cubes.remove_node(i)
            except: #Networkx sometimes throws a KeyError. No idea why, nothing pops up on Google, but it still works.
                pass
                
        self.accessories[:] = (x for x in self.accessories if x['x'] != x and x['y'] != y and x['z'] != z)
                
        self.calculate_corner_types()

    def parts_list(self):
        parts = {}
        parts['pipe'] = self.cubes.number_of_edges()

        parts['6way'] = self.get_corner_type_counts('6way')
        parts['corner'] = self.get_corner_type_counts('corner')
        parts['bottom_tway'] = self.get_corner_type_counts('bottom_tway')
        parts['bottom_4way'] = self.get_corner_type_counts('bottom_4way')

        parts['bed'] = self.get_accessory_counts('bed')
        parts['ramp_pipe'] = self.get_accessory_counts('ramp_pipe')
        parts['ramp_fabric'] = self.get_accessory_counts('ramp_fabric')
        parts['topper'] = self.get_accessory_counts('topper')
        parts['scratcher'] = self.get_accessory_counts('scratcher')
        parts['hut'] = self.get_accessory_counts('hut')
        
        return parts

    def get_graph(self):
        return json.dumps(list(set(self.cube_list)))

    def set_graph(self, data):
        data = json.loads(data)
        for cube in data:
            self.add(cube[0], cube[1], cube[2], cube[3])

    def get_accessories(self):
        return json.dumps(self.accessories)

    def set_accessories(self, data):
        if len(data) < 1:
            return
        self.accessories = json.loads(data)
