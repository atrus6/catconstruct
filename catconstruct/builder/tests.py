from decimal import Decimal as D

from django.test import TestCase

from .cart import Cart
from .construct import Tree
from .models import Cart as CartModel

# Create your tests here.
class TreeTests(TestCase):
    tree = Tree()
    def setUp(self):
        self.tree.clear()

    def tearDown(self):
        pass

    def test_add_single_node(self):
        self.tree.add_node(0,0,0)

        self.assertEqual(self.tree.cubes.number_of_nodes(), 1)

    def test_add_cube(self):
        self.tree.add_cube(0,0,0)
        
        correct_array = [
            "x:0 y:0 z:0",
            "x:1 y:0 z:0",
            "x:0 y:0 z:1",
            "x:1 y:0 z:1",
            "x:0 y:1 z:0",
            "x:1 y:1 z:0",
            "x:0 y:1 z:1",
            "x:1 y:1 z:1"]

        self.assertEqual(list(sorted(self.tree.cubes.nodes)), sorted(correct_array))
        self.assertEqual(self.tree.cubes.number_of_nodes(), 8)
        self.assertEqual(self.tree.cubes.number_of_edges(), 12)

    def test_add_cube_twice(self):
        self.tree.add_cube(0,0,0)
        self.tree.add_cube(0,0,0)
        
        correct_array = [
            "x:0 y:0 z:0",
            "x:1 y:0 z:0",
            "x:0 y:0 z:1",
            "x:1 y:0 z:1",
            "x:0 y:1 z:0",
            "x:1 y:1 z:0",
            "x:0 y:1 z:1",
            "x:1 y:1 z:1"]

        self.assertEqual(list(sorted(self.tree.cubes.nodes)), sorted(correct_array))
        self.assertEqual(self.tree.cubes.number_of_edges(), 12)

    def test_node_is_6way(self):
        self.tree.cubes.add_edge(0,1)
        self.tree.cubes.add_edge(0,2)
        self.tree.cubes.add_edge(0,3)
        self.tree.cubes.add_edge(0,4)
        self.tree.cubes.add_edge(0,5)
        self.tree.cubes.add_edge(0,6)

        self.assertEqual(self.tree.corner_type(0), '6way')

    def test_node_is_corner(self):
        a = self.tree.add_node(0,0,0)
        b = self.tree.add_node(0,1,0)
        c = self.tree.add_node(0,0,1)
        d = self.tree.add_node(1,0,1)

        self.tree.cubes.add_edge(a, b)
        self.tree.cubes.add_edge(a, c)
        self.tree.cubes.add_edge(a, d)

        self.tree.calculate_corner_types()
        self.assertEqual(self.tree.corner_type(a), 'corner')

class ConstructMath(TestCase):
    tree = Tree()

    def setUp(self):
        self.tree.clear()

    def test_single_cube(self):
        self.tree.add('cube', 0, 0, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 12)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 0)
        
    def test_two_cubes_stacked(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.add('cube', 0, 1, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 20)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['bottom_tway'], 4)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        
    def test_two_separate_cubes(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.add('cube', 0, 2, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 24)
        self.assertEqual(parts['corner'], 16)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 0)
        
    def test_add_invalid_cube(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.add('cube', 0, 0, 0)

        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 12)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 0)

    def test_add_ramp_on_top(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.add('ramp', 0, 1, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 15)
        self.assertEqual(parts['corner'], 6)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 2)
        self.assertEqual(parts['ramp_pipe'], 2)
        self.assertEqual(parts['ramp_fabric'], 1)
        
    def test_add_bed(self):
        self.tree.add('bed', 0, 0, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 12)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 0)
        self.assertEqual(parts['bed'], 1)
        
    def test_add_cubes2(self):
        self.tree.add('cube', 0, 0, 0)
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 12)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 0)
        
        self.tree.add('cube', 1, 0, 0)
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 20)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 4)
        
        self.tree.add('cube', 2, 0, 0)
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 28)
        self.assertEqual(parts['corner'], 8)
        self.assertEqual(parts['6way'], 0)
        self.assertEqual(parts['bottom_4way'], 0)
        self.assertEqual(parts['bottom_tway'], 8)
        
class DeconstructMath(TestCase):
    tree = Tree()

    def setUp(self):
        self.tree.clear()
        
    def test_remove_one_cube(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.remove(0, 0, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 0)
        self.assertEqual(parts['corner'], 0)
        
    def test_remove_two_cubes(self):
        self.tree.add('cube', 0, 0, 0)
        self.tree.add('cube', 1, 0, 0)
        self.tree.remove(1, 0, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 12)
        self.assertEqual(parts['corner'], 8)
        
    def test_remove_accessory(self):
        self.tree.add('bed', 0, 0, 0)
        self.tree.add('hut', 1, 0, 0)
        
        parts = self.tree.parts_list()
        self.assertEqual(parts['hut'], 1)
        
        self.tree.remove(0, 0, 0)
        self.tree.remove(1, 0, 0)
        
        parts = self.tree.parts_list()
        
        self.assertEqual(parts['pipe'], 0)
        self.assertEqual(parts['corner'], 0)
        self.assertEqual(parts['bed'], 0)
        self.assertEqual(parts['hut'], 0)
        
class ShopTests(TestCase):
    def setUp(self):
        x = CartModel(session='0')
        x.save()
        self.c = Cart()
        self.session = '0'
        
    def test_add_pipe(self):
        self.c.add_item('pipe', 1, self.session)
        
        self.assertEqual(self.c.get_pretax_total(self.session), D('0.30'))
        
    def test_add_two_pipes(self):
        self.c.add_item('pipe', 2, self.session)
        
        self.assertEqual(self.c.get_pretax_total(self.session), D('0.60'))
        
    def test_add_two_pipes_seperatly(self):
        self.c.add_item('pipe', 1, self.session)
        self.c.add_item('pipe', 1, self.session)
        
        self.assertEqual(self.c.get_pretax_total(self.session), D('0.60'))
        
    def test_remove_all(self):
        self.c.add_item('pipe', 10, self.session)
        self.assertEqual(self.c.get_pretax_total(self.session), D('3'))
        
        self.c.remove_item('pipe', 5, self.session)
        self.assertEqual(self.c.get_pretax_total(self.session), D('1.50'))
        
        self.c.remove_item('pipe', 5, self.session)
        self.assertEqual(self.c.get_pretax_total(self.session), D('0'))
        
    def test_get_items(self):
        self.c.add_item('pipe', 2, self.session)
        
        x = self.c.get_items(self.session)
        x = x[0]
        self.assertEqual(x.sku, 'pipe')
        self.assertEqual(x.quantity, 2)
        
    def test_accessories(self):
        self.c.add_item('pipe_4x', 1, self.session)
        self.assertEqual(self.c.get_pretax_total(self.session), D('3.00'))