from django import template

register = template.Library()

from builder.cart import Cart

@register.simple_tag(takes_context=True)
def cart_count(context):
    request = context['request']
    cart = Cart()
    return cart.get_number_of_items_in_cart(request.session.session_key)