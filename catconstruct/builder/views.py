from decimal import Decimal as D


from django.contrib.auth import login, authenticate
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView

from lazysignup.utils import is_lazy_user
import stripe

from .construct import Tree

from .cart import Cart, colors, items

from .forms import SignUpForm

from .models import TreeModel, Address, User, CartItem



class AccessoriesPage(TemplateView):
    template_name = 'builder/accessories.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['items'] = [items['6way_4x'], items['pipe_4x']]
        
        return context

class ShippingPage(CreateView):
    model = Address
    fields = ('name', 'address1', 'address2', 'city', 'state', 'zipcode')
    template_name = 'builder/shipping.html'
    success_url = '/checkout'
    
    def form_valid(self, form):
        print("Is lazy", is_lazy_user(self.request.user))
        form.instance.user = self.request.user
        
        return super(ShippingPage, self).form_valid(form)
    
class CheckoutPage(TemplateView):
    template_name = 'builder/checkout.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Set your secret key: remember to change this to your live secret key in production
        # See your keys here: https://dashboard.stripe.com/account/apikeys
        stripe.api_key = "sk_test_bhIsvD60DrEjEmnojkv7ZX0I"
        stripe.api_version = "2018-11-08; checkout_sessions_beta=v1"
        
        cart = Cart()
        cc = cart.get_items(self.request.session.session_key)
        item_list = []
        
        for c in cc:
            item_list.append(
                {
                    "amount": int(items[c.sku]['price'] * 100),
                    "quantity": c.quantity,
                    "name": items[c.sku]['name'],
                    "description": items[c.sku]['description'],
                    "currency":"usd"
                })
        
        address = Address.objects.filter(user=self.request.user).first()
        
        sid = stripe.checkout.Session.create(
            success_url="https://www.example.com/success",
            cancel_url="https://www.example.com/cancel",
            payment_method_types=["card"],
            line_items=item_list,
            payment_intent_data={"shipping": {
                "address": {
                        "line1":address.address1,
                        "line2":address.address2,
                        "city":address.city,
                        "country":"USA",
                        "state":address.state,
                        "postal_code":address.zipcode
                    },
                "name":address.name,
                
            }
                
            }
        )
        
        context['sid'] = sid.id
        return context
        
class CartPage(TemplateView):
    template_name = 'builder/cart.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        cart = Cart()
        
        cc = cart.get_items(self.request.session.session_key)
        pretax = cart.get_pretax_total(self.request.session.session_key)
        
        cwi = []
        
        for item in cc:
            cwi.append((item, items[item.sku]))
            
        if len(cc) == 0:
            context['empty'] = 'yes'
        else:
            context['empty'] = 'no'
            context['cart_contents'] = cwi
            context['cart_subtotal'] = pretax
            
            if pretax < D('10.00'):
                context['shipping'] = D('5.00')
            
            context['cart_tax'] = D(0.00)
            context['cart_total'] = pretax + context['cart_tax']
            
            if pretax < D('10.00'):
                context['cart_total'] = context['cart_total'] + D('5.00')
            
            
        return context

def add_to_cart(request):
    if not request.session.session_key:
        request.session.create()
        
    sku = request.POST.get('sku')
    quantity = request.POST.get('quantity')
    cart = Cart()
    cart.add_item(sku, quantity, request.session.session_key)
    return JsonResponse({'message':'Item added successfully.', 'cart_count':cart.get_number_of_items_in_cart(request.session.session_key)})

def get_tree(request):
    if not request.session.session_key:
        request.session.create()
    
    session = request.session.session_key
    
    if not TreeModel.objects.filter(key=session).exists():
        saved_tree = TreeModel()
        t = Tree()
        saved_tree.tree = t.get_graph()
        saved_tree.accessories = t.get_accessories()
        saved_tree.key = request.session.session_key
        saved_tree.save()
    else:
        saved_tree = TreeModel.objects.filter(key=session).first()
        
    tree = Tree()
    print('Tree', saved_tree.tree)
    tree.set_graph(saved_tree.tree)
    tree.set_accessories(saved_tree.accessories)
    
    return saved_tree, tree
    
def save_tree(saved_tree, tree):
    saved_tree.tree = tree.get_graph()
    saved_tree.accessories = tree.get_accessories()
    saved_tree.save()
    
def remove_cube(request):
    x = request.POST.get('x')
    y = request.POST.get('y')
    z = request.POST.get('z')
    
    saved_tree, tree = get_tree(request)
    tree.remove(x, y, z)
    save_tree(saved_tree, tree)
    
    return JsonResponse(tree.parts_list())

def add_cube(request):
    cube_type = request.POST.get('type')
    x = request.POST.get('x')
    y = request.POST.get('y')
    z = request.POST.get('z')
    
    saved_tree, tree = get_tree(request)
    
    tree.add(cube_type, x, y, z)
    
    save_tree(saved_tree, tree)
    
    return JsonResponse(tree.parts_list())
    
def get_cube_list(request):
    saved_tree, tree = get_tree(request)
    return JsonResponse({'items': tree.get_graph()})
    
def add_tree_to_cart(request):
    saved_tree, tree = get_tree(request)
    
    items = tree.parts_list()
    
    cart = Cart()
    
    for item in items:
        if items[item] > 0:
            cart.add_item(item, items[item], request.session.session_key)
    
    return JsonResponse({'message': 'All items added to cart.', 'cart_count': cart.get_number_of_items_in_cart(request.session.session_key)})
    
def reset_tree(request):
    if not request.session.session_key:
        request.session.create()
    
    try:
        tree = Tree()
        tree.clear()
        saved_tree = TreeModel.objects.filter(key=session.session_key).first()
        saved_tree.tree = tree.get_graph()
        saved_tree.accessories = tree.get_accesories()
        saved_tree.save()
    except:
        pass
    
    request.session.flush()
    return HttpResponseRedirect(reverse('builder'))
        
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def change_cart_quantity(request, pk, quantity):
    item = CartItem.objects.filter(id=pk).first()
    item.quantity = quantity
    item.save()
    
    return redirect('cart')

def remove_cart_item(request, pk):
    item = CartItem.objects.filter(id=pk).first()
    item.delete()
        
    return redirect('cart')
    
