from decimal import Decimal as D

from django.shortcuts import get_object_or_404

from .models import Cart as CartModel, CartItem

class ItemDefinition:
    def __init__(self, sku, name, variants, description, price):
        self.sku = sku
        self.name = name
        self.variants = variants
        self.description = description
        self.price = price
        
    def json(self):
        return {'sku': self.sku,
                'name':self.name,
                'variants': self.variants,
                'description': self.description,
                'price': self.price,
                }
        
colors = ['Red', 'Blue']

items = { '6way_4x': ItemDefinition('6way_4x', '4x 6way', variants=colors, description='A 4-pack of 6ways to expand a tree.', price=D('5.00')).json(),
          'pipe_4x': ItemDefinition('pipe_4x', '4x Pipe', variants=colors, description='A 4-pack of pipes to expand a tree.', price=D('3.00')).json(),
          'pipe' : ItemDefinition('pipe', 'Pipe', variants=colors, description='Cat Tree Structural Member.', price=D('0.30')).json(),
          'corner': ItemDefinition('corner', 'Corner', variants=colors, description='Connector for corners.', price=D('0.50')).json(),
          'bottom_4way': ItemDefinition('bottom_4way', '4way - Bottom Type', variants=colors, description='Connector for flat areas.', price=D('0.70')).json(),
          'bottom_tway': ItemDefinition('bottom_tway', '4way - Tee Type', variants=colors, description='Connector for edges.', price=D('0.70')).json(),
          '6way': ItemDefinition('6way', '6way Connector', variants=colors, description='Connector for all directions.', price=D('1.00')).json(),
          'topper': ItemDefinition('topper', 'Topper', variants=None, description='Simple fabric platform.', price=D('5.00')).json(),
          'hut': ItemDefinition('hut', 'Hut', variants=None, description='Fabric box to hide in.', price=D('7.50')).json(),
          'bed': ItemDefinition('bed', 'Bed', variants=None, description='Comfy bed to lie in.', price=D('12.00')).json(),
          'scratcher': ItemDefinition('scratcher', 'Scratcher', variants=None, description='Scratching post that replaces a pipe.', price=D('6.50')).json(),
          'ramp-kit': ItemDefinition('ramp-kit', 'Ramp Kit', variants=colors, description='A curved ramp to reach the next level.', price=D('12.00')).json(),
        }

class Cart:
    def add_item(self, item, quantity, sid):
        if not CartModel.objects.filter(session=sid).exists():
            x = CartModel(session=sid)
            x.save()
        cart = CartModel.objects.filter(session=sid).first()
        
        if cart.cart.filter(sku=item).count() > 0:
            ci = cart.cart.filter(sku=item).first()
            ci.quantity = ci.quantity + int(quantity)
            ci.save()
        else:
            c = CartItem(cart=cart, sku=item, quantity=quantity)
            c.save()
            
    def remove_item(self, item, quantity, sid):
        cart = CartModel.objects.filter(session=sid).first()
        
        cartlist = cart.cart.filter(sku=item)
        
        if cartlist.count() > 0:
            ci = cartlist.first()
            ci.quantity = ci.quantity - quantity
            
            if ci.quantity <= 0:
                ci.delete()
            else:
                ci.save()
                
    def get_items(self, session):
        print('Get Items', session)
        if not CartModel.objects.filter(session=session).exists():
            return {}
        
        cart = CartModel.objects.filter(session=session).first()
        
        cartlist = CartItem.objects.filter(cart=cart)
        
        return cartlist
        
    def get_number_of_items_in_cart(self, session):
        if not CartModel.objects.filter(session=session).exists():
            return {}
        
        cart = CartModel.objects.filter(session=session).first()
        
        cartlist = CartItem.objects.filter(cart=cart)
        cc = 0
        
        if CartItem.objects.filter(cart=cart).count() == 0:
            return 0

        for item in cartlist:
            cc += item.quantity
        
        return cc
        
    def get_pretax_total(self, sid):
        total = D(0)
        
        c = CartModel.objects.filter(session=sid).first()
        
        for cartitem in CartItem.objects.filter(cart=c):
            try:
                total = total + (items[cartitem.sku].price * cartitem.quantity)
            except:
                pass
            
        return total
        