# Generated by Django 2.1.5 on 2019-01-28 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builder', '0002_remove_treemodel_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='treemodel',
            name='accessories',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
