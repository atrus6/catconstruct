# Generated by Django 2.0.10 on 2019-02-11 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builder', '0008_treemodel_key'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartitem',
            name='per_item_price',
        ),
        migrations.AddField(
            model_name='cartitem',
            name='color',
            field=models.CharField(blank=True, choices=[('Blue', 'Blue'), ('Green', 'Green'), ('Red', 'Red'), ('Yellow', 'Yellow')], max_length=15),
        ),
    ]
