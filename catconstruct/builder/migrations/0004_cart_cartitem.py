# Generated by Django 2.0.10 on 2019-02-04 11:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('builder', '0003_treemodel_accessories'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.IntegerField()),
                ('quantity', models.IntegerField()),
                ('per_item_price', models.DecimalField(decimal_places=2, max_digits=7)),
                ('cart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='builder.Cart')),
            ],
        ),
    ]
