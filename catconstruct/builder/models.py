from decimal import Decimal as D

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser

from django.db import models

color_list = (('Blue', 'Blue'),
              ('Green', 'Green'),
              ('Red', 'Red'),
              ('Yellow', 'Yellow'))

class User(AbstractUser):
    sid = models.TextField(blank=True)
    pass

class TreeModel(models.Model):
    key = models.TextField()
    tree = models.TextField()
    accessories = models.TextField()

class Cart(models.Model):
    session = models.TextField()

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name='cart')
    sku = models.TextField()
    quantity = models.IntegerField()
    color = models.CharField(max_length=15, choices=color_list, blank=True)
    
class Address(models.Model):
    user = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    address1 = models.CharField(max_length=1024)
    address2 = models.CharField(max_length=1024)
    city = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    zipcode = models.CharField(max_length=12)